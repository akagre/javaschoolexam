package com.tsystems.javaschool.tasks.subsequence;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public class Subsequence {
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        try {
            List<String> master = new ArrayList<String>(x);
            List<String> candidate = new ArrayList<String>(y);
            candidate.retainAll(master); // Candidate now contains only element that exist in master list.
                                         // That means that master can be built by deleting some elements (include zero) or can't be,
                                         // if candidate have another order of elements or less elements than it's necessary

            if (candidate.size() < master.size())  // Master can't be built from smaller list
                return false;
            int j = 0;
            for (int i = 0; i < master.size(); i++) { // Compare elements from different lists
                                                      // If they not equal maybe current element of candidate can be removed and next element of candidate will be fit

                if (i + j >= candidate.size())  //Index out of bound means that candidate have not fit element or less elements than it's necessary
                    return false;
                while ((i + j < candidate.size()) && master.get(i) != candidate.get(i + j)) {
                    j++; //
                }
            }
            return true;
        }
        catch (NullPointerException e) {
            throw new IllegalArgumentException();
        }
    }
}


