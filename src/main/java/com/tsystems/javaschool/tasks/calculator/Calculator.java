package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.math.BigDecimal;

public class Calculator {

    //Method converting infix notation into reverse polish notation
    private static String getPolNotation (String infix, Map <Character,Integer> operators){
        StringBuffer result = new StringBuffer(); // String accumulating  polish notation
        List<Character> symbols = new ArrayList<>();
        LinkedList<Character> stack = new LinkedList<>(); //Stack containing operators and brackets
        for (int i = 0 ; i < infix.length() ; i++) { // Split input expression to char
            if (infix.charAt(i) != ' ') {
                symbols.add(infix.charAt(i));
            }
        }
        for (Character c : symbols) { // For each symbol call method deciding is symbol part of number, operator or something else
                analizeInputChar(c, result, stack, operators);
        }
        while (!stack.isEmpty()) { // Add remaining operators to output string
            addSpace(result); // See method description little bit lower
            result.append(stack.pop());
        }
        return result.toString();
    }

    //Simple method to make decision about adding spaces. Spaces need to separate units in reverse polish notation. Call this method every time meet symbols different from digit(that means that previous part was number or another operator)
    private static void addSpace (StringBuffer result) {
        if ((result.length() != 0) && !(result.charAt(result.length()- 1) == ' ')) {
            result.append(' ');
        }
    }
    private static void analizeInputChar(Character c, StringBuffer result, LinkedList<Character> stack,Map <Character,Integer> operators) throws IllegalArgumentException, NoSuchElementException {
        if (c.equals('(')){
            addSpace(result);
            stack.push(c);
        }
        else if (c.equals(')')){
            addSpace(result);
            boolean isLeftBracket = false;
            Character sym;
            while (!isLeftBracket){
                try {
                    sym = stack.pop();
                }
                catch (NoSuchElementException e) {
                    throw e; // Throw exception, if input expression have unbalanced number of brackets
                }
                if (sym.equals('(')){ // Pop all operators from stack till left bracket (include left bracket)
                    isLeftBracket = true;
                }
                else {
                    result.append(sym); //Add operators to output except left bracket
                    addSpace(result);
                }

            }
        }
        else if (operators.containsKey(c)){
            addSpace(result);
            while (!stack.isEmpty() && (operators.containsKey(stack.getFirst())) && (operators.get(c) <= operators.get(stack.getFirst()))) {
                result.append(stack.pop()); // Push all operators with higher priority into output string
                addSpace(result);

            }
            stack.push(c);
        }
        else if (Character.isDigit(c) || c.equals('.')) {
            result.append(c); // Parts of numbers adding straight to output string
        }
        else
            throw new IllegalArgumentException(); // Throw exception, if input expression have some indefinite symbols
    }

    public String evaluate(String statement){

        Map <Character, Integer> availableOperators = new HashMap<Character,Integer>(); // Make collection of operators with value of priority
        availableOperators.put('+',1);
        availableOperators.put('-',1);
        availableOperators.put('*',2);
        availableOperators.put('/',2);

        LinkedList<Double> stack = new LinkedList<>();// Stack for parsing expression in reverse polish notation
        String resultString = new String(); //String contains result of calculation
        try {
            String polStatment = getPolNotation(statement, availableOperators); // Change notation
            Scanner scanner = new Scanner(polStatment);
            try {
                while (scanner.hasNext()) {
                    Object element = scanner.next();
                    if (availableOperators.containsKey(element.toString().charAt(0))) { // In case of operator pop last two numbers from stack and compute them
                        Double b = stack.pop();
                        Double a = stack.pop();
                        if (element.equals("+"))
                            stack.push(a + b);
                        if (element.equals("-"))
                            stack.push(a - b);
                        if (element.equals("*"))
                            stack.push(a * b);
                        if (element.equals("/"))
                            stack.push(a / b);
                    } else {
                        stack.push(Double.parseDouble(element.toString())); // In case of numbers put them into stack
                    }
                }
                Double d = stack.pop(); // Now stack contains only one value - result of calculation
                double resultDouble = new BigDecimal(d).setScale(4, BigDecimal.ROUND_HALF_DOWN).doubleValue(); // Rounding float value to 4 digits after dot
                if (resultDouble == (int) resultDouble) // Cast to integer if result has no fractional part
                    resultString = resultString + (int)resultDouble;
                else
                    resultString = resultString + resultDouble;
            } catch (NumberFormatException e) {
                return null;
            }
        }
        catch (IllegalArgumentException e){
            System.out.println("Incorrect expression, use only digits, simple math operators and dot as decimal delimiter");
            return null;
        }
        catch (NoSuchElementException e){
            System.out.println("Incorrect expression");
            return null;
        }
        catch (NullPointerException e) {
            System.out.println("No input expression");
            return null;
        }

        return resultString;
    }
}
