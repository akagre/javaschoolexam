package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class DuplicateFinder {

    public boolean process(File sourceFile, File targetFile) {
        try {
            if (!targetFile.exists()) // Creating new output file in case it's missing
                targetFile.createNewFile();
            Map<String,Integer> lines = new TreeMap<String,Integer>(); // Collection for storage output text lines. TreeMap is sorted and unique, so it's fine for that task
            Scanner scan = new Scanner(sourceFile);
            while (scan.hasNextLine()){
                String line = scan.nextLine();
                if (lines.containsKey(line))
                    lines.replace(line,lines.get(line)+1); // Increase value in case of existing line
                else
                    lines.put(line,1); // Add new line
            }
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(targetFile, true)));
            for (String line : lines.keySet()) {
                writer.println(line + "[" + lines.get(line) + "]");
            }
            writer.close();
            return true;
        }
        catch (FileNotFoundException e){
            System.out.println("No input/output file");
            throw new IllegalArgumentException();
        }
        catch (NullPointerException e){
            System.out.println("No input/output file");
            throw new IllegalArgumentException();
        }
        catch (IOException e){
            System.out.println("Input/output error");
            return false;
        }
    }
}
